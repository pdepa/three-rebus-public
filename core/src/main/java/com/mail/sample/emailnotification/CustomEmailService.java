package com.mail.sample.emailnotification;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;


@SlingServlet(paths="/bin/emailservice")
public class CustomEmailService extends SlingAllMethodsServlet {

	
	private static final long serialVersionUID = 8795673847499208743L;

	protected void doGet(SlingHttpServletRequest request,SlingHttpServletResponse response)
			throws IOException {
			
		try {
			String from = "pavani@threeuk.com";
			
			
			Properties properties = System.getProperties();
			properties.put("mail.smtp.host", "127.0.0.1");
			properties.put("mail.smtp.port", "125");
			properties.put("mail.smtp.uname", "pavani@threeuk.com");
			properties.put("mail.smtp.pswd", "maY@2017");
			Session session = Session.getDefaultInstance(properties);
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(request.getParameter("toAddress")));
			message.setSubject(request.getParameter("subject"));
			message.setText(request.getParameter("message"));

			Transport.send(message);
			response.getWriter().println("Sent message successfully....");
		}
			
		catch (Exception ex) {
			response.getWriter().println("Exception"+ex);
			ex.printStackTrace();
		}
	}

	public static boolean isEmpty(String aValue) {
		return (aValue == null || aValue.trim().length() == 0);
	}
}

